$( document ).ready(function() {
    let width = $(".cycle").width();
    let width_box = $(".cycle-box").width();
    $(".cycle").height(width);
    $(".cycle-box").height(width_box);
    $('.popup').hide();
    $('.close').click(function (e) { 
        e.preventDefault();
        $('.popup').hide();
    });
    $('.btn-rule').click(function (e) { 
        e.preventDefault();
        $('#popup-rules').show();
    });
    $('#contact-submit').click(function (e) { 
        e.preventDefault();
        console.log("object")
        $('#popup-spins').show();
    });
});

$( window ).resize(function() {
    let width = $(".cycle").width();
    let width_box = $(".cycle-box").width();
    $(".cycle").height(width);
    $(".cycle-box").height(width_box);
});